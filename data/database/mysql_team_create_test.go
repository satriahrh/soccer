package database_test

import (
	"context"
	"fmt"
	"math/rand"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"soccer/data"
)

func (suite *SuiteMysql) TestTeamCreate() {
	suite.Run("TrxBegin/Error", func() {
		ctx := context.TODO()

		suite.sqlMock.ExpectBegin().WillReturnError(fmt.Errorf("unexpected error"))

		_, err := suite.mysql.TeamCreate(ctx, data.Team{})
		assert.EqualError(suite.T(), err, "unexpected error")
	})
	suite.Run("ExecContext", func() {
		setup := func() (data.Team, context.Context) {
			return data.Team{
				Name: "Satria",
			}, context.TODO()
		}
		suite.Run("Error", func() {
			team, ctx := setup()
			suite.sqlMock.ExpectBegin()
			suite.sqlMock.ExpectExec("INSERT INTO teams").
				WithArgs(team.Name).
				WillReturnError(fmt.Errorf("unexpected error"))
			suite.sqlMock.ExpectRollback()

			_, err := suite.mysql.TeamCreate(ctx, team)
			assert.EqualError(suite.T(), err, "unexpected error")
		})
		suite.Run("Success", func() {
			teamId := rand.Uint64()
			team, ctx := setup()
			suite.sqlMock.ExpectBegin()
			suite.sqlMock.ExpectExec("INSERT INTO teams").
				WithArgs(team.Name).
				WillReturnResult(sqlmock.NewResult(int64(teamId), 1))
			suite.sqlMock.ExpectCommit()

			actualTeam, err := suite.mysql.TeamCreate(ctx, team)
			expectedTeam := *(&team)
			expectedTeam.ID = teamId
			if assert.NoError(suite.T(), err) {
				assert.EqualValues(suite.T(), expectedTeam, actualTeam)
			}
		})
	})
}
