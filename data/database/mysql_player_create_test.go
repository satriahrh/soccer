package database_test

import (
	"context"
	"fmt"
	"math/rand"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"soccer/data"
)

func (suite *SuiteMysql) TestPlayerCreate() {
	suite.Run("TrxBegin/Error", func() {
		ctx := context.TODO()

		suite.sqlMock.ExpectBegin().WillReturnError(fmt.Errorf("unexpected error"))

		_, err := suite.mysql.PlayerCreate(ctx, data.Player{})
		assert.EqualError(suite.T(), err, "unexpected error")
	})
	suite.Run("ExecContext", func() {
		setup := func() (data.Player, context.Context) {
			return data.Player{
				Name:   "Satria",
				TeamID: rand.Uint64() / 1000,
			}, context.TODO()
		}
		suite.Run("Error", func() {
			player, ctx := setup()
			suite.sqlMock.ExpectBegin()
			suite.sqlMock.ExpectExec("INSERT INTO players").
				WithArgs(player.Name, player.TeamID).
				WillReturnError(fmt.Errorf("unexpected error"))
			suite.sqlMock.ExpectRollback()

			_, err := suite.mysql.PlayerCreate(ctx, player)
			assert.EqualError(suite.T(), err, "unexpected error")
		})
		suite.Run("Success", func() {
			playerId := rand.Uint64()
			player, ctx := setup()
			suite.sqlMock.ExpectBegin()
			suite.sqlMock.ExpectExec("INSERT INTO players").
				WithArgs(player.Name, player.TeamID).
				WillReturnResult(sqlmock.NewResult(int64(playerId), 1))
			suite.sqlMock.ExpectCommit()

			actualPlayer, err := suite.mysql.PlayerCreate(ctx, player)
			expectedPlayer := *(&player)
			expectedPlayer.ID = playerId
			if assert.NoError(suite.T(), err) {
				assert.EqualValues(suite.T(), expectedPlayer, actualPlayer)
			}
		})
	})
}
