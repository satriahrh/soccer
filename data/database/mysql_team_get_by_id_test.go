package database_test

import (
	"context"
	"math/rand"
	"strconv"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"soccer/data"
)

func (suite *SuiteMysql) TestTeamGetByID() {
	teamId := rand.Uint64() /1000

	suite.sqlMock.ExpectQuery(`SELECT id, name FROM teams WHERE id = \?`).
		WithArgs(strconv.FormatUint(teamId, 10)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "name"}).AddRow(teamId, "Persatuan Sepak Bola"))

	teamActual, err := suite.mysql.TeamGetByID(context.TODO(), strconv.FormatUint(teamId, 10))
	if assert.NoError(suite.T(), err) {
		assert.EqualValues(suite.T(), data.Team{
			ID:      teamId,
			Name:    "Persatuan Sepak Bola",
		}, teamActual)
	}
}
