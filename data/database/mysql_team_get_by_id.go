package database

import (
	"context"

	"soccer/data"
)

func (mysql *MySql) TeamGetByID(ctx context.Context, id string) (team data.Team, err error) {
	row := mysql.db.QueryRowContext(ctx, `SELECT id, name FROM teams WHERE id = ?`, id)
	return team, row.Scan(&team.ID, &team.Name)
}
