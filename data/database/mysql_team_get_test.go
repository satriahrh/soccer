package database_test

import (
	"context"
	"fmt"
	"math/rand"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"soccer/data"
)

func (suite *SuiteMysql) TestTeamGet() {
	setup := func() (context.Context, int, int) {
		return context.TODO(), 10, 0
	}
	suite.Run("QueryContext/Error", func() {
		ctx, limit, offset := setup()
		suite.sqlMock.ExpectQuery(
			`SElECT id, name FROM teams LIMIT \?, \?`).WithArgs(offset, limit).
			WillReturnError(fmt.Errorf("unexpected error"))

		_, err := suite.mysql.TeamGet(ctx, limit, offset)
		assert.EqualError(suite.T(), err, "unexpected error")
	})
	suite.Run("QueryContext/Success", func() {
		suite.Run("ScanError", func() {
			ctx, limit, offset := setup()
			suite.sqlMock.ExpectQuery(
				`SElECT id, name FROM teams LIMIT \?, \?`).WithArgs(offset, limit).
				WillReturnRows(
					sqlmock.NewRows([]string{"id", "name",}).AddRow("a", "b"),
				)

			_, err := suite.mysql.TeamGet(ctx, limit, offset)
			assert.Error(suite.T(), err)
		})
		suite.Run("Success", func() {
			ctx, limit, offset := setup()
			teams := []data.Team{
				{ID: rand.Uint64() / 1000, Name: "Persatuan Sepak Bola"},
			}
			suite.sqlMock.ExpectQuery(
				`SElECT id, name FROM teams LIMIT \?, \?`).WithArgs(offset, limit).
				WillReturnRows(
					sqlmock.NewRows([]string{"id", "name"}).AddRow(teams[0].ID, teams[0].Name),
				)
			teamsActual, err := suite.mysql.TeamGet(ctx, limit, offset)
			if assert.NoError(suite.T(), err) {
				assert.EqualValues(suite.T(), teams, teamsActual)
			}
		})
	})
}
