package lib

import (
	"context"

	"github.com/sirupsen/logrus"
	"soccer/data"
)

func (lib *Library) TeamGetDetailAndPlayers(ctx context.Context, teamId string) (team data.Team, err error) {
	if team, err = lib.database.TeamGetByID(ctx, teamId); err != nil {
		logrus.WithContext(ctx).Error(err)
	} else {
		if team.Players, err = lib.database.PlayerGetByTeamID(ctx, teamId, 11, 0); err != nil {
			logrus.WithContext(ctx).Error(err)
		} else if len(team.Players) == 0 {
			team.Players = []data.Player{}
		}
	}
	return
}
