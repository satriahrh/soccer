package route_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httptest"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"soccer/data"
)

func (suite *SuiteRoute) TestPlayersCreate() {
	suite.Run("JsonDecode/Error", func() {
		playerString := ``
		req := httptest.NewRequest("POST", "/players", bytes.NewBufferString(playerString))

		requestRecorder := httptest.NewRecorder()
		suite.router.ServeHTTP(requestRecorder, req)

		assert.Equal(suite.T(), http.StatusUnprocessableEntity, requestRecorder.Code)
		assert.Equal(suite.T(), fmt.Sprintln(`{"error":"EOF"}`), requestRecorder.Body.String())
	})
	suite.Run("JsonDecode/Success/PlayerCreate", func() {
		playerCreation := func() (data.Player, string) {
			playerName := "Satria"
			teamId := rand.Uint64()
			playerString := fmt.Sprintf(`{"name":"%v","team_id":%v}`, playerName, teamId)
			var player data.Player
			_ = json.Unmarshal([]byte(playerString), &player)
			return player, playerString
		}
		suite.Run("Error", func() {
			player, playerString := playerCreation()

			req := httptest.NewRequest("POST", "/players", bytes.NewBufferString(playerString))

			suite.libraryMock.On("PlayerCreate", mock.Anything, player).
				Return(player, fmt.Errorf("expected error"))

			requestRecorder := httptest.NewRecorder()
			suite.router.ServeHTTP(requestRecorder, req)

			assert.Equal(suite.T(), http.StatusUnprocessableEntity, requestRecorder.Code)
			assert.Equal(suite.T(), fmt.Sprintln(`{"error":"expected error"}`), requestRecorder.Body.String())
		})
		suite.Run("Success", func() {
			player, playerString := playerCreation()

			req := httptest.NewRequest("POST", "/players", bytes.NewBufferString(playerString))

			suite.libraryMock.On("PlayerCreate", mock.Anything, player).
				Return(player, nil)

			requestRecorder := httptest.NewRecorder()
			suite.router.ServeHTTP(requestRecorder, req)

			assert.Equal(suite.T(), http.StatusCreated, requestRecorder.Code)
			assert.Equal(suite.T(), fmt.Sprintln(`{"message":"player has been created"}`), requestRecorder.Body.String())
		})
	})
}
