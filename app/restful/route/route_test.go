package route_test

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/suite"
	"soccer/app/restful/route"
	"soccer/mocks"
)

type SuiteRoute struct {
	suite.Suite
	libraryMock *mocks.LibraryInterface
	router      http.Handler
}

func (suite *SuiteRoute) SetupTest() {
	suite.libraryMock = &mocks.LibraryInterface{}
	suite.router = route.New(suite.libraryMock)
}

func TestRoute(t *testing.T) {
	suite.Run(t, new(SuiteRoute))
}
