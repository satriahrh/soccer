package route

type teams struct {
	*handler
}

func teamsResource(handler *handler) {
	teams := teams{handler}
	teams.router.Path("").Methods("POST").HandlerFunc(teams.create)
	teams.router.Path("").Methods("GET").HandlerFunc(teams.read)
	teams.router.Path("/{id}").Methods("GET").HandlerFunc(teams.readByID)
}
