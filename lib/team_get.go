package lib

import (
	"context"

	"soccer/data"
)

func (lib *Library) TeamGet(ctx context.Context, limit, offset int) ([]data.Team, error) {
	return lib.database.TeamGet(ctx, limit, offset)
}
