package route_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"soccer/data"
)

func (suite *SuiteRoute) TestTeamsCreate() {
	url := "/teams"
	suite.Run("JsonDecode/Error", func() {
		teamString := ``
		req := httptest.NewRequest("POST", url, bytes.NewBufferString(teamString))

		requestRecorder := httptest.NewRecorder()
		suite.router.ServeHTTP(requestRecorder, req)

		assert.Equal(suite.T(), http.StatusUnprocessableEntity, requestRecorder.Code)
		assert.Equal(suite.T(), fmt.Sprintln(`{"error":"EOF"}`), requestRecorder.Body.String())
	})
	suite.Run("JsonDecode/Success/TeamCreate", func() {
		teamCreation := func() (data.Team, string) {
			teamName := fmt.Sprintf("Persatuan Indonesia Number %v", time.Now().UnixNano())
			teamString := fmt.Sprintf(`{"name":"%v"}`, teamName)
			var team data.Team
			_ = json.Unmarshal([]byte(teamString), &team)
			return team, teamString
		}
		suite.Run("Error", func() {
			team, teamString := teamCreation()

			req := httptest.NewRequest("POST", url, bytes.NewBufferString(teamString))

			suite.libraryMock.On("TeamCreate", mock.Anything, team).
				Return(team, fmt.Errorf("expected error"))

			requestRecorder := httptest.NewRecorder()
			suite.router.ServeHTTP(requestRecorder, req)

			assert.Equal(suite.T(), http.StatusUnprocessableEntity, requestRecorder.Code)
			assert.Equal(suite.T(), fmt.Sprintln(`{"error":"expected error"}`), requestRecorder.Body.String())
		})
		suite.Run("Success", func() {
			team, teamString := teamCreation()

			req := httptest.NewRequest("POST", url, bytes.NewBufferString(teamString))

			suite.libraryMock.On("TeamCreate", mock.Anything, team).
				Return(team, nil)

			requestRecorder := httptest.NewRecorder()
			suite.router.ServeHTTP(requestRecorder, req)

			assert.Equal(suite.T(), http.StatusCreated, requestRecorder.Code)
			assert.Equal(suite.T(), fmt.Sprintln(`{"message":"team has been created"}`), requestRecorder.Body.String())
		})
	})
}
