package main

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"soccer/app/restful/route"
	"soccer/data/database"
	"soccer/lib"
)

func main() {
	logrus.SetReportCaller(true)
	logrus.SetFormatter(&logrus.JSONFormatter{})

	signalChannel := make(chan os.Signal)
	signal.Notify(signalChannel, os.Interrupt, os.Kill, syscall.SIGTERM, syscall.SIGINT)

	err := godotenv.Load()
	if err != nil {
		logrus.Warning(err)
	}

	db, err := sql.Open("mysql", os.Getenv("CLEARDB_DATABASE_URL"))
	if err != nil {
		logrus.Fatal(err)
	}
	mysql := database.ConnectMysql(db)

	library := lib.New(mysql)

	router := route.New(library)

	server := &http.Server{
		Addr:         fmt.Sprintf(":%v", os.Getenv("PORT")),
		Handler:      router,
		ReadTimeout:  3 * time.Second,
		WriteTimeout: 5 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	go func() {
		if err := server.ListenAndServe(); err != nil {
			logrus.Info(err)
		}
	}()
	logrus.Info("starting the server, will be available in port :" + os.Getenv("PORT"))

	<-signalChannel

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer func() {
		cancel()
	}()
	if err := server.Shutdown(ctx); err != nil {
		logrus.WithContext(ctx).Error(err)
	}
}
