package route

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"soccer/lib"
)

type handler struct {
	router  *mux.Router
	library lib.LibraryInterface
}

type routeResource func(*handler)

var resources = map[string]routeResource{
	"/teams":   teamsResource,
	"/players": playersResource,
}

func New(library lib.LibraryInterface) http.Handler {
	router := mux.NewRouter()

	for path := range resources {
		resources[path](&handler{router.PathPrefix(path).Subrouter(), library})
	}

	return handlers.LoggingHandler(logrus.StandardLogger().Out, router)
}

func (h *handler) getLimitAndOffsetWithDefault(r *http.Request) (limit, offset int) {
	var err error
	if limit, err = strconv.Atoi(r.URL.Query().Get("limit")); err != nil {
		limit = 10
	}
	if offset, err = strconv.Atoi(r.URL.Query().Get("offset")); err != nil {
		offset = 0
	}
	return
}

func (h *handler) responseMessage(ctx context.Context, w http.ResponseWriter, httpStatusCode int, message string) {
	h.responseData(ctx, w, httpStatusCode, struct {
		Message string `json:"message"`
	}{
		message,
	})
}

func (h *handler) responseError(ctx context.Context, w http.ResponseWriter, httpStatusCode int, err error) {
	h.responseData(ctx, w, httpStatusCode, struct {
		Error string `json:"error"`
	}{
		err.Error(),
	})
}

func (h *handler) responseData(ctx context.Context, w http.ResponseWriter, httpStatusCode int, i interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpStatusCode)
	if err := json.NewEncoder(w).Encode(i); err != nil {
		logrus.WithContext(ctx).Error(err)
	}
}
