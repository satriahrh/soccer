package database

import (
	"context"
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
	"github.com/sirupsen/logrus"
)

type MySql struct {
	db *sql.DB
}

func ConnectMysql(db *sql.DB) *MySql {
	return &MySql{db: db}
}

func (mysql *MySql) trxBegin(ctx context.Context) (*sql.Tx, error) {
	return mysql.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
		ReadOnly:  false,
	})
}

func (mysql *MySql) trxFinalize(tx *sql.Tx, err error) error {
	if err != nil {
		if errRollback := tx.Rollback(); errRollback != nil {
			logrus.Error(errRollback)
			return errRollback
		}
		return err
	}
	return tx.Commit()
}
