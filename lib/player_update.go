package lib

import (
	"context"

	"soccer/data"
)

func (lib *Library) PlayerUpdate(ctx context.Context, playerID string, player data.Player) (playerUpdated data.Player, err error) {
	return lib.database.PlayerUpdate(ctx, playerID, player)
}
