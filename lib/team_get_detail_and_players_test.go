package lib_test

import (
	"context"
	"fmt"
	"math/rand"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"soccer/data"
	"soccer/lib"
	"soccer/mocks"
)

type SuiteTeamGetDetailAndPlayers struct {
	suite.Suite
	mck     *mocks.DatabaseInterface
	library lib.LibraryInterface
}

func (suite *SuiteTeamGetDetailAndPlayers) SetupTest() {
	suite.mck = &mocks.DatabaseInterface{}
	suite.library = lib.New(suite.mck)
}

func (suite *SuiteTeamGetDetailAndPlayers) TestTeamGetByID() {
	teamId := rand.Uint64()
	teamName := "Tim Bola A"
	playerName := "Satria"

	suite.Run("Error", func() {
		ctx := context.WithValue(context.TODO(), "timestamp", time.Now())

		suite.mck.On("TeamGetByID", ctx, strconv.FormatUint(teamId, 10)).
			Return(data.Team{}, fmt.Errorf("expected error"))

		_, err := suite.library.TeamGetDetailAndPlayers(ctx, strconv.FormatUint(teamId, 10))
		assert.EqualError(suite.T(), err, "expected error")
	})
	suite.Run("Success", func() {
		expectedTeam := data.Team{
			ID:   teamId,
			Name: teamName,
		}

		successTeam := func(suite *SuiteTeamGetDetailAndPlayers) (context.Context) {
			ctx := context.WithValue(context.TODO(), "timestamp", time.Now())

			suite.mck.On("TeamGetByID", ctx, strconv.FormatUint(teamId, 10)).
				Return(expectedTeam, nil)

			return ctx
		}

		suite.Run("PlayerGetByTeamID", func() {
			suite.Run("SuccessFueled", func() {
				ctx := successTeam(suite)

				expectedPlayers := []data.Player{
					{
						ID:     rand.Uint64(),
						Name:   playerName,
						TeamID: teamId,
					},
				}
				suite.mck.On("PlayerGetByTeamID", ctx, strconv.FormatUint(teamId, 10), 11, 0).
					Return(expectedPlayers, nil)

				pointerOfExptedTeam := &expectedTeam
				finalExpectedTeam := *pointerOfExptedTeam
				finalExpectedTeam.Players = expectedPlayers
				actualTeam, err := suite.library.TeamGetDetailAndPlayers(ctx, strconv.FormatUint(teamId, 10))
				if assert.NoError(suite.T(), err) {
					assert.EqualValues(suite.T(), finalExpectedTeam, actualTeam)
				}
			})
			suite.Run("SuccessEmpty", func() {
				ctx := successTeam(suite)

				var expectedPlayers []data.Player
				suite.mck.On("PlayerGetByTeamID", ctx, strconv.FormatUint(teamId, 10), 11, 0).
					Return(expectedPlayers, nil)

				pointerOfExptedTeam := &expectedTeam
				finalExpectedTeam := *pointerOfExptedTeam
				finalExpectedTeam.Players = []data.Player{}
				actualTeam, err := suite.library.TeamGetDetailAndPlayers(ctx, strconv.FormatUint(teamId, 10))
				if assert.NoError(suite.T(), err) {
					assert.EqualValues(suite.T(), finalExpectedTeam, actualTeam)
				}
			})
			suite.Run("Error", func() {
				ctx := successTeam(suite)

				suite.mck.On("PlayerGetByTeamID", ctx, strconv.FormatUint(teamId, 10), 11, 0).
					Return(nil, fmt.Errorf("expected error"))
				_, err := suite.library.TeamGetDetailAndPlayers(ctx, strconv.FormatUint(teamId, 10))
				assert.EqualError(suite.T(), err, "expected error")
			})
		})
	})
}

func TestLibrary_TeamGetDetailAndPlayers(t *testing.T) {
	suite.Run(t, new(SuiteTeamGetDetailAndPlayers))
}
