package route

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
	"soccer/data"
)

func (h *players) create(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var player data.Player
	if err := json.NewDecoder(r.Body).Decode(&player); err != nil {
		logrus.WithContext(ctx).Error(err)
		h.responseError(ctx, w, http.StatusUnprocessableEntity, err)
	} else if _, err := h.library.PlayerCreate(ctx, player); err != nil {
		logrus.WithContext(ctx).Error(err)
		h.responseError(ctx, w, http.StatusUnprocessableEntity, err)
	} else {
		h.responseMessage(ctx, w, http.StatusCreated, "player has been created")
	}
}
