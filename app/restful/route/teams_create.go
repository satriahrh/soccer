package route

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
	"soccer/data"
)

func (h *teams) create(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var team data.Team
	if err := json.NewDecoder(r.Body).Decode(&team); err != nil {
		logrus.WithContext(ctx).Error(err)
		h.responseError(ctx, w, http.StatusUnprocessableEntity, err)
	} else if _, err := h.library.TeamCreate(ctx, team); err != nil {
		logrus.WithContext(ctx).Error(err)
		h.responseError(ctx, w, http.StatusUnprocessableEntity, err)
	} else {
		h.responseMessage(ctx, w, http.StatusCreated, "team has been created")
	}
}
