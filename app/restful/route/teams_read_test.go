package route_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httptest"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"soccer/data"
)

func (suite *SuiteRoute) TestTeamsRead() {
	suite.Run("WithoutLimitOffset/TeamGet/Error", func() {
		url := "/teams"
		limit, offset := 10, 0
		req := httptest.NewRequest("GET", url, nil)

		suite.libraryMock.On("TeamGet", mock.Anything, limit, offset).
			Return(nil, fmt.Errorf("expected error"))

		requestRecorder := httptest.NewRecorder()
		suite.router.ServeHTTP(requestRecorder, req)

		assert.Equal(suite.T(), http.StatusUnprocessableEntity, requestRecorder.Code)
		assert.Equal(suite.T(), fmt.Sprintln(`{"error":"expected error"}`), requestRecorder.Body.String())
	})
	suite.Run("WithLimitOffset/TeamGet/Success", func() {
		limit, offset := 3, 0
		url := fmt.Sprintf("/teams?limit=%v&offset=%v", limit, offset)
		req := httptest.NewRequest("GET", url, nil)

		expectedTeams := []data.Team{
			{
				ID:   rand.Uint64(),
				Name: "Persatuan Sepak Bola",
			},
		}
		suite.libraryMock.On("TeamGet", mock.Anything, limit, offset).
			Return(expectedTeams, nil)

		requestRecorder := httptest.NewRecorder()
		suite.router.ServeHTTP(requestRecorder, req)

		expectedTeamsJson, _ := json.Marshal(expectedTeams)
		expectedTeamsBuffer := bytes.NewBuffer(expectedTeamsJson)
		expectedTeamsBuffer.WriteString("\n")
		assert.Equal(suite.T(), http.StatusOK, requestRecorder.Code)
		assert.EqualValues(suite.T(), expectedTeamsBuffer, requestRecorder.Body)
	})
}
