package database

import (
	"context"
	"database/sql"

	"github.com/sirupsen/logrus"
	"soccer/data"
)

func (mysql *MySql) TeamGet(ctx context.Context, limit, offset int) (teams []data.Team, err error) {
	var rows *sql.Rows
	if rows, err = mysql.db.QueryContext(ctx,
		`SElECT id, name FROM teams LIMIT ?, ?`,
		offset, limit,
	); err != nil {
		logrus.WithContext(ctx).Error(err)
	} else {
		for rows.Next() {
			var team data.Team
			if err = rows.Scan(&team.ID, &team.Name); err != nil {
				logrus.WithContext(ctx).Error(err)
				return
			}
			teams = append(teams, team)
		}
	}
	return
}
