package route

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"soccer/data"
)

func (h *players) update(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	params := mux.Vars(r)
	playerID := params["id"]
	var player data.Player
	if err := json.NewDecoder(r.Body).Decode(&player); err != nil {
		logrus.WithContext(ctx).Error(err)
		h.responseError(ctx, w, http.StatusUnprocessableEntity, err)
	} else if player, err = h.library.PlayerUpdate(ctx, playerID, player); err != nil {
		logrus.WithContext(ctx).Error(err)
		h.responseError(ctx, w, http.StatusUnprocessableEntity, err)
	} else {
		h.responseMessage(ctx, w, http.StatusOK, "player has been updated")
	}
}
