package database

import (
	"context"

	"soccer/data"
)

type DatabaseInterface interface {
	PlayerCreate(ctx context.Context, player data.Player) (data.Player, error)
	PlayerGetByTeamID(ctx context.Context, teamId string, limit, offset int) ([]data.Player, error)
	PlayerUpdate(ctx context.Context, id string, player data.Player) (data.Player, error)
	TeamCreate(ctx context.Context, team data.Team) (data.Team, error)
	TeamGet(ctx context.Context, limit, offset int) ([]data.Team, error)
	TeamGetByID(ctx context.Context, id string) (data.Team, error)
}
