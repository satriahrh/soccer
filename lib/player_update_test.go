package lib_test

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"soccer/data"
	"soccer/lib"
	"soccer/mocks"
)

func TestLibrary_PlayerUpdate(t *testing.T) {
	playerId := rand.Uint64()
	playerName := "Satria"
	teamId := rand.Uint64()
	newTeamId := rand.Uint64()

	mck := &mocks.DatabaseInterface{}
	library := lib.New(mck)

	expectedPlayer := data.Player{
		ID:     rand.Uint64(),
		Name:   playerName,
		TeamID: teamId,
	}
	ctx := context.TODO()
	mck.On("PlayerUpdate", ctx, strconv.FormatUint(playerId, 10), data.Player{
		Name:   playerName,
		TeamID: newTeamId,
	}).Return(expectedPlayer, nil)

	playerString := fmt.Sprintf(`{"name":"%v","team_id":%v}`, playerName, newTeamId)
	var player data.Player
	_ = json.Unmarshal([]byte(playerString), &player)
	actualPlayer, err := library.PlayerUpdate(ctx, strconv.FormatUint(playerId, 10), player)
	if assert.NoError(t, err) {
		assert.Equal(t, expectedPlayer, actualPlayer)
	}
}
