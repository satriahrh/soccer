package database

import (
	"context"
	"database/sql"
	"strconv"

	"github.com/sirupsen/logrus"
	"soccer/data"
)

func (mysql *MySql) PlayerUpdate(ctx context.Context, id string, player data.Player) (playerUpdated data.Player, err error) {
	var playerId uint64
	if playerId, err = strconv.ParseUint(id, 10, 64); err != nil {
		logrus.WithContext(ctx).Error(err)
		return
	}

	var trx *sql.Tx
	if trx, err = mysql.trxBegin(ctx); err != nil {
		logrus.WithContext(ctx).Error(err)
		return
	}
	defer func() {
		err = mysql.trxFinalize(trx, err)
	}()

	if _, err = trx.ExecContext(ctx,
		`UPDATE players SET name=?, team_id=? WHERE id = ? `,
		player.Name, player.TeamID, playerId,
	); err != nil {
		logrus.WithContext(ctx).Error(err)
	} else {
		playerUpdated = player
		playerUpdated.ID = playerId
	}
	return
}
