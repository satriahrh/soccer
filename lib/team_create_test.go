package lib_test

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
	"soccer/data"
	"soccer/lib"
	"soccer/mocks"
)

func TestLibrary_TeamCreate(t *testing.T) {
	teamName := "Tim Bola A"

	mck := &mocks.DatabaseInterface{}
	library := lib.New(mck)

	expectedTeam := data.Team{
		ID:      rand.Uint64(),
		Name:    teamName,
		Players: nil,
	}
	ctx := context.TODO()
	mck.On("TeamCreate", ctx, data.Team{
		Name:    teamName,
		Players: nil,
	}).Return(expectedTeam, nil)

	teamString := fmt.Sprintf(`{"name":"%v"}`, teamName)
	var team data.Team
	_ = json.Unmarshal([]byte(teamString), &team)
	actualTeam, err := library.TeamCreate(ctx, team)
	if assert.NoError(t, err) {
		assert.Equal(t, expectedTeam, actualTeam)
	}
}
