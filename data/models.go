package data

type PlayerPosition uint8

type Team struct {
	ID      uint64   `json:"id"`
	Name    string   `json:"name"`
	Players []Player `json:"players,omitempty"`
}

type Player struct {
	ID     uint64 `json:"id"`
	Name   string `json:"name"`
	TeamID uint64 `json:"team_id"`
}
