CREATE TABLE players (
    id BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    team_id BIGINT UNSIGNED,
    CONSTRAINT fk_players_team_id FOREIGN KEY (team_id) REFERENCES teams (id)
)