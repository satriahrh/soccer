package database_test

import (
	"context"
	"fmt"
	"math/rand"
	"strconv"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"soccer/data"
)

func (suite *SuiteMysql) TestPlayerGetByTeamID() {
	setup := func() (context.Context, string, int, int) {
		return context.TODO(), strconv.FormatUint(rand.Uint64()/1000, 10), 10, 0
	}
	suite.Run("QueryContext/Error", func() {
		ctx, teamId, limit, offset := setup()
		suite.sqlMock.ExpectQuery(
			`SElECT id, name, team_id FROM players WHERE team_id = \? LIMIT \?, \?`).WithArgs(teamId, offset, limit).
			WillReturnError(fmt.Errorf("unexpected error"))

		_, err := suite.mysql.PlayerGetByTeamID(ctx, teamId, limit, offset)
		assert.EqualError(suite.T(), err, "unexpected error")
	})
	suite.Run("QueryContext/Success", func() {
		suite.Run("ScanError", func() {
			ctx, teamId, limit, offset := setup()
			suite.sqlMock.ExpectQuery(
				`SElECT id, name, team_id FROM players WHERE team_id = \? LIMIT \?, \?`).WithArgs(teamId, offset, limit).
				WillReturnRows(
					sqlmock.NewRows([]string{"id", "name", "team_id"}).AddRow("a", "b", "c"),
				)

			_, err := suite.mysql.PlayerGetByTeamID(ctx, teamId, limit, offset)
			assert.Error(suite.T(), err)
		})
		suite.Run("Success", func() {
			ctx, teamId, limit, offset := setup()
			teamIdUint, _ := strconv.ParseUint(teamId, 10, 64)
			players := []data.Player{
				{ID: rand.Uint64() / 1000, Name: "Satria", TeamID: teamIdUint},
			}
			suite.sqlMock.ExpectQuery(
				`SElECT id, name, team_id FROM players WHERE team_id = \? LIMIT \?, \?`).WithArgs(teamId, offset, limit).
				WillReturnRows(
					sqlmock.NewRows([]string{"id", "name", "team_id"}).AddRow(players[0].ID, players[0].Name, players[0].TeamID),
				)
			playersActual, err := suite.mysql.PlayerGetByTeamID(ctx, teamId, limit, offset)
			if assert.NoError(suite.T(), err) {
				assert.EqualValues(suite.T(), players, playersActual)
			}
		})
	})
}
