package lib

import (
	"context"

	"soccer/data"
)

func (lib *Library) TeamCreate(ctx context.Context, team data.Team) (data.Team, error) {
	return lib.database.TeamCreate(ctx, team)
}
