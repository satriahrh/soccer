package route

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

func (h *teams) readByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	params := mux.Vars(r)
	teamId := params["id"]
	if team, err := h.library.TeamGetDetailAndPlayers(ctx, teamId); err != nil {
		logrus.WithContext(ctx).Error(err)
		h.responseError(ctx, w, http.StatusUnprocessableEntity, err)
	} else {
		h.responseData(ctx, w, http.StatusOK, team)
	}
}
