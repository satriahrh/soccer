package database_test

import (
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"soccer/data/database"
)

type SuiteMysql struct {
	suite.Suite
	mysql   database.DatabaseInterface
	sqlMock sqlmock.Sqlmock
}

func (suite *SuiteMysql) SetupTest() {
	logrus.SetReportCaller(true)
	logrus.SetFormatter(&logrus.JSONFormatter{})
	db, sqlMock, _ := sqlmock.New()
	suite.sqlMock = sqlMock
	suite.mysql = database.ConnectMysql(db)
}

func TestMysql(t *testing.T) {
	suite.Run(t, new(SuiteMysql))
}
