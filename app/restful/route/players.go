package route

type players struct {
	*handler
}

func playersResource(handler *handler) {
	players := players{handler}
	players.router.Path("").Methods("POST").HandlerFunc(players.create)
	players.router.Path("/{id}").Methods("PATCH").HandlerFunc(players.update)
}
