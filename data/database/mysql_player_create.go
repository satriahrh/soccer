package database

import (
	"context"
	"database/sql"

	"github.com/sirupsen/logrus"
	"soccer/data"
)

func (mysql *MySql) PlayerCreate(ctx context.Context, player data.Player) (playerInserted data.Player, err error) {
	var trx *sql.Tx
	if trx, err = mysql.trxBegin(ctx); err != nil {
		logrus.WithContext(ctx).Error(err)
		return
	}
	defer func() {
		err = mysql.trxFinalize(trx, err)
	}()

	var result sql.Result
	if result, err = trx.ExecContext(ctx,
		`INSERT INTO players (name, team_id) VALUES (?, ?)`,
		player.Name, player.TeamID,
	); err != nil {
		logrus.WithContext(ctx).Error(err)
	} else {
		playerInserted = player
		playerId64, _ := result.LastInsertId()
		playerInserted.ID = uint64(playerId64)
	}
	return
}
