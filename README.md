# soccer

## Requirements

 - [Go 1.12](https://golang.org/doc/go1.12)
 - [golang-migrate](https://github.com/golang-migrate/migrate#cli-usage) for database migration
 - [Mysql 5.7](https://dev.mysql.com/downloads/mysql/5.7.html)
 
## Setup

 - initiate required environment variables in `.env` and adjust desired values
    ```shell script
    cp env.sample .env
    ```
   
 - make sure you have created the database in mysql. To make database migration, run following command (you probably need to export `MYSQL_DSN` or passing it to this command)
    ```shell script
    make db-migrate
    ```

 - Build the restful API
    ```shell script
    make build
    ```
 
 - Run the API 
     ```shell script
     ./bin/restful
    ```
