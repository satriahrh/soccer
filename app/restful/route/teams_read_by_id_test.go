package route_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"strconv"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"soccer/data"
)

func (suite *SuiteRoute) TestTeamsReadByID() {
	suite.Run("TeamGetDetailAndPlayers/Error", func() {
		teamId := rand.Uint64()
		url := fmt.Sprintf("/teams/%v", strconv.FormatUint(teamId, 10))
		req := httptest.NewRequest("GET", url, nil)

		suite.libraryMock.On("TeamGetDetailAndPlayers", mock.Anything, strconv.FormatUint(teamId, 10)).
			Return(data.Team{}, fmt.Errorf("expected error"))

		requestRecorder := httptest.NewRecorder()
		suite.router.ServeHTTP(requestRecorder, req)

		assert.Equal(suite.T(), http.StatusUnprocessableEntity, requestRecorder.Code)
		assert.Equal(suite.T(), fmt.Sprintln(`{"error":"expected error"}`), requestRecorder.Body.String())
	})
	suite.Run("TeamGetDetailAndPlayers/Success", func() {
		teamId := rand.Uint64()
		url := fmt.Sprintf("/teams/%v", strconv.FormatUint(teamId, 10))
		req := httptest.NewRequest("GET", url, nil)

		expectedTeam := data.Team{
			ID:   rand.Uint64(),
			Name: "Persatuan Sepak Bola",
			Players: []data.Player{
				{ID: rand.Uint64(), Name: "Satria"},
			},
		}
		suite.libraryMock.On("TeamGetDetailAndPlayers", mock.Anything, strconv.FormatUint(teamId, 10)).
			Return(expectedTeam, nil)

		requestRecorder := httptest.NewRecorder()
		suite.router.ServeHTTP(requestRecorder, req)

		expectedTeamJson, _ := json.Marshal(expectedTeam)
		expectedTeamBuffer := bytes.NewBuffer(expectedTeamJson)
		expectedTeamBuffer.WriteString("\n")
		assert.Equal(suite.T(), http.StatusOK, requestRecorder.Code)
		assert.EqualValues(suite.T(), expectedTeamBuffer, requestRecorder.Body)
	})
}
