package lib

import (
	"context"

	"soccer/data"
)

func (lib *Library) PlayerCreate(ctx context.Context, player data.Player) (data.Player, error) {
	return lib.database.PlayerCreate(ctx, player)
}
