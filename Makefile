db-create-migration:
	migrate create -ext sql -dir db/mysql $(NAME)
db-migrate:
	migrate -database "mysql://$(MYSQL_DSN)" -source file://db/mysql up
db-rollback:
	migrate -database "mysql://$(MYSQL_DSN)" -source file://db/mysql down 1
build:
	go build -o bin/restful app/restful/main.go