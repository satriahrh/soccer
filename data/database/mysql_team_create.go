package database

import (
	"context"
	"database/sql"

	"github.com/sirupsen/logrus"
	"soccer/data"
)

func (mysql *MySql) TeamCreate(ctx context.Context, team data.Team) (teamInserted data.Team, err error) {
	var trx *sql.Tx
	if trx, err = mysql.trxBegin(ctx); err != nil {
		logrus.WithContext(ctx).Error(err)
		return
	}
	defer func() {
		err = mysql.trxFinalize(trx, err)
	}()

	var result sql.Result
	if result, err = trx.ExecContext(ctx,
		`INSERT INTO teams (name) VALUES (?)`,
		team.Name,
	); err != nil {
		logrus.WithContext(ctx).Error(err)
	} else {
		teamInserted = team
		teamId64, _ := result.LastInsertId()
		teamInserted.ID = uint64(teamId64)
	}
	return
}
