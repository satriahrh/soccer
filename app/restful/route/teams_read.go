package route

import (
	"net/http"

	"github.com/sirupsen/logrus"
)

func (h *teams) read(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	limit, offset := h.getLimitAndOffsetWithDefault(r)
	if teams, err := h.library.TeamGet(ctx, limit, offset); err != nil {
		logrus.WithContext(ctx).Error(err)
		h.responseError(ctx, w, http.StatusUnprocessableEntity, err)
	} else {
		h.responseData(ctx, w, http.StatusOK, teams)
	}
}
