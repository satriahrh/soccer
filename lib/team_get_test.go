package lib_test

import (
	"context"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
	"soccer/data"
	"soccer/lib"
	"soccer/mocks"
)

func TestLibrary_TeamGetTest(t *testing.T) {
	teamName := "Tim Bola A"
	limit, offset := 3, 0

	mck := &mocks.DatabaseInterface{}
	library := lib.New(mck)

	expectedTeams := []data.Team{
		{
			ID:   rand.Uint64(),
			Name: teamName,
		},
	}
	ctx := context.TODO()
	mck.On("TeamGet", ctx, limit, offset).
		Return(expectedTeams, nil)

	actualTeams, err := library.TeamGet(ctx, limit, offset)
	if assert.NoError(t, err) {
		assert.EqualValues(t, expectedTeams, actualTeams)
	}
}
