package lib

import (
	"context"

	"soccer/data"
	"soccer/data/database"
)

type Library struct {
	database database.DatabaseInterface
}

type LibraryInterface interface {
	TeamCreate(ctx context.Context, team data.Team) (data.Team, error)
	TeamGet(ctx context.Context, limit, offset int) ([]data.Team, error)
	TeamGetDetailAndPlayers(ctx context.Context, teamId string) (data.Team, error)
	PlayerCreate(ctx context.Context, player data.Player) (data.Player, error)
	PlayerUpdate(ctx context.Context, playerID string, player data.Player) (data.Player, error)
}

func New(database database.DatabaseInterface) *Library {
	return &Library{database: database}
}
