package database

import (
	"context"
	"database/sql"

	"github.com/sirupsen/logrus"
	"soccer/data"
)

func (mysql *MySql) PlayerGetByTeamID(ctx context.Context, teamID string, limit, offset int) (players []data.Player, err error) {
	var rows *sql.Rows
	if rows, err = mysql.db.QueryContext(ctx,
		`SElECT id, name, team_id FROM players WHERE team_id = ? LIMIT ?, ?`,
		teamID, offset, limit,
	); err != nil {
		logrus.WithContext(ctx).Error(err)
	} else {
		for rows.Next() {
			var player data.Player
			if err = rows.Scan(&player.ID, &player.Name, &player.TeamID); err != nil {
				logrus.WithContext(ctx).Error(err)
				return
			}
			players = append(players, player)
		}
	}
	return
}
