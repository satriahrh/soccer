package database_test

import (
	"context"
	"fmt"
	"math/rand"
	"strconv"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"soccer/data"
)

func (suite *SuiteMysql) TestPlayerUpdate() {
	suite.Run("Parsing", func() {
		playerId := ""
		ctx := context.TODO()

		_, err := suite.mysql.PlayerUpdate(ctx, playerId, data.Player{})
		assert.EqualError(suite.T(), err, "strconv.ParseUint: parsing \"\": invalid syntax")
	})
	suite.Run("Transaction", func() {
		setup := func() (context.Context, data.Player, uint64) {
			return context.TODO(), data.Player{
				Name:   "Satria",
				TeamID: rand.Uint64() / 1000,
			}, rand.Uint64() / 1000
		}
		suite.Run("TrxBegin/Error", func() {
			ctx, player, playerId := setup()

			suite.sqlMock.ExpectBegin().WillReturnError(fmt.Errorf("unexpected error"))

			_, err := suite.mysql.PlayerUpdate(ctx, strconv.FormatUint(playerId, 10), player)
			assert.EqualError(suite.T(), err, "unexpected error")
		})
		suite.Run("ExecContext", func() {
			suite.Run("Error", func() {
				ctx, player, playerId := setup()
				suite.sqlMock.ExpectBegin()
				suite.sqlMock.ExpectExec("UPDATE players").
					WithArgs(player.Name, player.TeamID, playerId).
					WillReturnError(fmt.Errorf("unexpected error"))
				suite.sqlMock.ExpectRollback()

				_, err := suite.mysql.PlayerUpdate(ctx, strconv.FormatUint(playerId, 10), player)
				assert.EqualError(suite.T(), err, "unexpected error")
			})
			suite.Run("Success", func() {
				ctx, player, playerId := setup()
				suite.sqlMock.ExpectBegin()
				suite.sqlMock.ExpectExec("UPDATE players").
					WithArgs(player.Name, player.TeamID, playerId).
					WillReturnResult(sqlmock.NewResult(0, 1))
				suite.sqlMock.ExpectCommit()

				actualPlayer, err := suite.mysql.PlayerUpdate(ctx, strconv.FormatUint(playerId, 10),player)
				expectedPlayer := *(&player)
				expectedPlayer.ID = playerId
				if assert.NoError(suite.T(), err) {
					assert.EqualValues(suite.T(), expectedPlayer, actualPlayer)
				}
			})
		})
	})

}
